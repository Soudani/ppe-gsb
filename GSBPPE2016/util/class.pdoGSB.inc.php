﻿<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application lafleur
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * @package default
 * @author Patrice Grand
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGSB
{   		
      	private static $serveur='mysql:host=172.17.0.9';
      	private static $bdd='dbname=gsb_ppe2016';
      	private static $user='anass' ;    		
      	private static $mdp='btssio' ;	
		private static $monPdo;
		private static $monPdoGSB = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	private function __construct()
	{
		PdoGSB::$monPdo = new PDO(PdoGSB::$serveur.';'.PdoGSB::$bdd, PdoGSB::$user, PdoGSB::$mdp);
		PdoGSB::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGSB::$monPdo = null;
	}
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : PdoGSB = PdoGSB::getPdoGSB();
 * @return l'unique objet de la classe PdoGSB
 */
	public static function getPdoGSB()
	{
		if(PdoGSB::$monPdoGSB == null)
		{
			PdoGSB::$monPdoGSB= new PdoGSB();
		}
		return PdoGSB::$monPdoGSB;
	}

	
	public function verifierUser($login, $mdp){
		$req = "select count(*) as nb from employes where login='".$login."' and mdp='".$mdp."'";
		$res = PdoGSB::$monPdo->query($req);
		$LaLigne = $res->fetch();
		$nombre = $LaLigne['nb'];
		if ($nombre == 1){
			$_SESSION["login"] = $_POST["login"];
			return true;
		}else{
			return false;
		}
	}

	public function getAllInfo($login,$mdp){
		$req = "select * from employes where login='".$login."' AND mdp='".$mdp."';";

		$res = PdoGSB::$monPdo->query($req);
		$lesInfos = $res->fetch();
		return $lesInfos;

	}

	public function getLesEvenements()
	{
		$lesEvenements=array();
		$req = "select * from evenement";
		$res = PdoGSB::$monPdo->query($req);
		$unEvenement = $res->fetchAll();
		$lesEvenements[] = $unEvenement;
		return $lesEvenements;
	}

/*
	

	public function rechercher($b){

		$lesEmployes=array();
		$req = "select * from employes where nom like '".$b."%'";
		$res = PdoGSB::$monPdo->query($req);
		$unEmploye = $res->fetchAll();
		$lesEmployes[] = $unEmploye;
		return $lesEmployes;
	}

	public function employe()
	{
		$lesEmployes=array();
		$req = "select * from employes";
		$res = PdoGSB::$monPdo->query($req);
		$unEmploye = $res->fetchAll();
		$lesEmployes[] = $unEmploye;
		return $lesEmployes;
	}

	public function getInfo($id){
		$lesInfos=array();
		$req = "select * from employes where idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$uneInfo = $res->fetchAll();
		$lesInfos[] = $uneInfo;
		return $lesInfos;
	}

	public function getAllInfo($login,$mdp){
		$req = "select * from employes where login='".$login."' AND mdp='".$mdp."';";

		$res = PdoGSB::$monPdo->query($req);
		$lesInfos = $res->fetch();
		return $lesInfos;

	}

	public function getEventPeople($id){
		$lesEvenements=array();
		$req = "select * from evenement inner join convoquer on evenement.idE=convoquer.idE where convoquer.idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$unEvenement = $res->fetchAll();
		$lesEvenements[] = $unEvenement;
		return $lesEvenements;
	}

	public function getServicePeople($id){
		$req = "select libelle from service inner join employes on employes.idS=service.idS  where employes.idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
		$libel = $res->fetchAll();
		return $libel[0];
	}

	public function getIdEmploye($nom){
		$req = "select idEm from employes where nom = '".$nom."'";
		$res = PdoGSB::$monPdo->query($req);
		$resul = $res->fetch();
		return $resul[0];
	}

	public function Ajouterevent($id, $event){
		$req = "insert into participe values (".$id.", ".$event.")";
		$res = PdoGSB::$monPdo->query($req);
	}

	public function Deleteevent($id, $event){
		$req = "delete from participe where idE = ".$event." and idEm = ".$id;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function getInfosEvent($info)
	{
		$lesInfos=array();
		$req = "select * from evenement where idE =".$info;
		//echo $req;
		$res = PdoGSB::$monPdo->query($req);
		$infoEvent = $res->fetchAll();
		$lesInfos[] = $infoEvent;
		//echo var_dump($lesInfos);
		return $lesInfos;
	}

	public function getEmployesEvent($idevent)
	{
		$lesEmployes=array();
		$req = "select nom from employes inner join convoquer on employes.idEm=convoquer.idEm where convoquer.idE = ".$idevent;
		//echo "<br/>".$req;
		$res = PdoGSB::$monPdo->query($req);
		$Employe = $res->fetchAll();
		$lesEmployes[] = $Employe;
		return $lesEmployes;
	}

	public function ValidModif($id, $inti, $lieu, $date, $heure)
	{
		$req = "UPDATE evenement SET lieu='".$lieu."', dateE='".$date."', heureE='".$heure."', Description='".$inti."' WHERE idE=".$id;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function SupprEvent($id)
	{
		$req = "DELETE from evenement WHERE idE=".$id;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function AddEvent($inti, $lieu, $date, $heure)
	{
		$req = "select max(idE) from evenement";
		$res = PdoGSB::$monPdo->query($req);
		$id = $res->fetchAll();
		$id[0][0]++;
		$req = "INSERT INTO evenement VALUES (".$id[0][0].",'".$lieu."','".$date."','".$heure."','".$inti."',1,1)";
		echo $req;
		$res = PdoGSB::$monPdo->query($req);
	}

	public function ScriptMail()
	{
		ini_set("SMTP","80.12.242.10" );
		ini_set("sendmail_from","louis.aubertot@gmail.com" );
		$date = date("Y")."-".date("m")."-".(date("d")+2);
		$req = "select * from evenement where dateE < '".$date."'";
		$res = PdoGSB::$monPdo->query($req);
		$event = $res->fetchAll();
		$lesEvenements[] = $event;
		foreach ($lesEvenements[0] as $c) {
			$message = "N'oubliez pas la ".$c["Description"]." du : ".$c["dateE"].". Lieu : ".$c["lieu"].". Heure : ".$c["heureE"].". On compte sur vous !";
			$message = wordwrap($message, 70, "\r\n");
			$req = "select login from employes inner join participe on employes.idEm=participe.idEm where idE = ".$c['idE'];
			$res = PdoGSB::$monPdo->query($req);
			if($res){
				$mail = $res->fetchAll();
				$lesMails[] = $mail;
				foreach ($lesMails[0] as $m){
					$email = $m["login"];
					mail($email, 'Réunion', $message);
				}
			}
		}

	}
						 /*****************************************************\
						 *                                                     *
						 *                        END                          *
						 *                                                     *
						 \*****************************************************/
}
?>